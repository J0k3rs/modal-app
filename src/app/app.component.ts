import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'modal-app';
  public editProfileForm: FormGroup;

  userList = [{
    id: '1',
    firstName: 'Oumar',
    lastName: 'Keita',
    username: 'Oumarooto',
    email: 'keimar.ok@gmail.com',
  },
    {
      id: '2',
      firstName: 'Lassana',
      lastName: 'Traore',
      username: 'Tiekoun',
      email: 'lassa09@yahoo.com',
    },
    {
      id: '3',
      firstName: 'Daouda',
      lastName: 'Coulibaly',
      username: 'Davski',
      email: 'daouada.coulibaly@gmail.com',
    },
    {
      id: '4',
      firstName: 'Mohamed',
      lastName: 'Berthe',
      username: 'Moh',
      email: 'momo@gmail.com',
    }];


  constructor(private fb: FormBuilder, private modalService: NgbModal) {
  }

  ngOnInit(): void {
    this.editProfileForm = this.fb.group({
      firstName: [''],
      lastName: [''],
      username: [''],
      email: ['']
    });
  }

  openModal(targetModal, user): any {

    this.modalService.open(targetModal, {
        centered: true,
        backdrop: 'static'
      }
    );

    this.editProfileForm.patchValue({
      firstName: user.firstName,
      lastName: user.lastName,
      username: user.username,
      email: user.email
    });
  }


  onSubmit(): any {
    this.modalService.dismissAll();
    console.log('rest:', this.editProfileForm.getRawValue());
  }

}
